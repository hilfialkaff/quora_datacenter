"""
Solution for Quora datacenter cooling challenge.

Answer for large test case: 301716
"""

datacenter = [] # Two dimensional array of the datacenters
width = 0 # Width of datacenters
height = 0 # Height of datacenters
num_rooms = 0 # Number of rooms that we own
visited = {} # Dicts of rooms that have been visited

"""
Count the number of rooms that we own in the datacenters.
"""
def count_num_rooms():
    num_rooms = 0
    for i in range(height):
        for j in range(width):
            num_rooms += (datacenter[i][j] != 1)

    return num_rooms

"""
Find where to start connecting the ducts.
"""
def find_start_point():
    for i in range(height):
        for j in range(width):
            if datacenter[i][j] == 2:
                return [i, j]

"""
Check if we own the room and it has not been visited.
"""
def is_room_free(cur_x, cur_y):
    return visited[(cur_x, cur_y)] == False and datacenter[cur_y][cur_x] != 1

"""
All nodes degree should at least be 2 except the destination and rooms that
we do not own.
"""
def check_nodes_degree(cur_x, cur_y):
    for i in range(height):
        for j in range(width):
            degree = 0

            if datacenter[i][j] == 3 or not is_room_free(j, i):
                continue

            if j > 0 and \
                (is_room_free(j - 1, i) or (j - 1 == cur_x and i == cur_y)):
                degree += 1
            if i > 0 and \
                (is_room_free(j, i - 1) or (j == cur_x and i - 1 == cur_y)):
                degree += 1
            if j < (width - 1) and \
                (is_room_free(j + 1, i) or (j + 1 == cur_x and i == cur_y)):
                degree += 1
            if i < (height - 1) and \
                (is_room_free(j, i + 1) or (j == cur_x and i + 1 == cur_y)):
                degree += 1

            if degree < 2:
                return 0

    return 1

"""
Recursion helper function for checking reachability of the nodes.
"""
def _check_reachability(reachable, cur_x, cur_y):
    reachable[(cur_x, cur_y)] = True

    if cur_x > 0 and (cur_x - 1, cur_y) not in reachable:
        _check_reachability(reachable, cur_x - 1, cur_y)
    if cur_y > 0 and (cur_x, cur_y - 1) not in reachable:
        _check_reachability(reachable, cur_x, cur_y - 1)
    if cur_x < (width - 1) and (cur_x + 1, cur_y) not in reachable:
        _check_reachability(reachable, cur_x + 1, cur_y)
    if cur_y < (height - 1) and (cur_x, cur_y + 1) not in reachable:
        _check_reachability(reachable, cur_x, cur_y + 1)

"""
Ensure all nodes are still reachable from each other.
"""
def check_reachability():
    reachable = {}
    start_x = -1
    start_y = -1

    for i in range(height):
        for j in range(width):
            if is_room_free(j, i):
                start_x, start_y = j, i
            else:
                reachable[(j, i)] = True

    if start_x == -1:
        return 0

    _check_reachability(reachable, start_x, start_y)

    for i in range(height):
        for j in range(width):
            if (j, i) not in reachable:
                return 0
    del reachable
    return 1

"""
Main function to count the # of ways to connect all rooms with single cooling duct.
"""
def count_ways(cur_x, cur_y, num_visited):
    if num_visited == num_rooms:
        if datacenter[cur_y][cur_x] == 3:
            return 1
        else:
            return 0
    elif check_nodes_degree(cur_x, cur_y) == False:
        return 0
    elif num_visited > 0.2 * float(num_rooms) and check_reachability() == False:
        return 0
    else:
        num_ways = 0
        if cur_x > 0 and is_room_free(cur_x - 1, cur_y):
            visited[(cur_x - 1, cur_y)] = True
            num_ways += count_ways(cur_x - 1, cur_y, num_visited + 1)
            visited[(cur_x - 1, cur_y)] = False
        if cur_y > 0 and is_room_free(cur_x, cur_y - 1):
            visited[(cur_x, cur_y - 1)] = True
            num_ways += count_ways(cur_x, cur_y - 1, num_visited + 1)
            visited[(cur_x, cur_y - 1)] = False
        if cur_x < (width - 1) and is_room_free(cur_x + 1, cur_y):
            visited[(cur_x + 1, cur_y)] = True
            num_ways += count_ways(cur_x + 1, cur_y, num_visited + 1)
            visited[(cur_x + 1, cur_y)] = False
        if cur_y < (height - 1) and is_room_free(cur_x, cur_y + 1):
            visited[(cur_x, cur_y + 1)] = True
            num_ways += count_ways(cur_x, cur_y + 1, num_visited + 1)
            visited[(cur_x, cur_y + 1)] = False

        return num_ways

"""
Parse input from STDIN.
"""
def parse_input():
    global width
    global height
    global datacenter

    stdin = raw_input()
    width, height = [int(_) for _ in stdin.split(' ')]

    while True:
        try:
            stdin = raw_input()
        except EOFError:
            break

        datacenter.append([int(_) for _ in stdin.split(' ')])

if __name__ == '__main__':
    visited = {}
    parse_input()

    num_rooms = count_num_rooms()
    start_y, start_x = find_start_point()
    for i in range(height):
        for j in range(width):
            visited[(j, i)] = False
    visited[(start_x, start_y)] = True
    num_visited = 1

    num_ways = count_ways(start_x, start_y, num_visited)
    print "num ways:", num_ways
